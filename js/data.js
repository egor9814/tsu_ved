
function __make_styled_div(styling, content) {
    return '<div style="' + styling + '">' + content + '</div>';
}

function make_data_header(title, end, bg, bg_offset) {
    var background = "background";
    background += bg === null ? ": #DEDEDE" : ("-image: url('img/" + bg + "')");
    background += ';';

    var tail = end ? "background-image: url('img/hdr_end.gif');" : "";

    var begin = "background-image: url('img/hdr_start.gif');";

    var size = "height: 26px; ";
    var bg_size = size + "width: " + (end ? "340" : "380") + "px;";
    size += "width: 26px;";

    var floating = "float: left;";

    var begin_style = begin + " " + size + " " + floating;
    var mid_style = background + " " + bg_size + " " + floating + ' color: #686868; font-family: "Noto Sans", serif;';
    var end_style = tail === "" ? "" : (tail + " " + size + " " + floating);

    var begin_div = __make_styled_div(begin_style, '');
    var mid_div = __make_styled_div(mid_style, __make_styled_div('padding: 2px 0 0 10px', title));
    var end_div = end_style === "" ? "" : __make_styled_div(end_style, '');

    return __make_styled_div('', begin_div + mid_div + end_div) + "<br>";
}


function __data1_title() {
    return "<p>Компания ВэД рада приветствовать Вас на своем сайте!</p>";
}

function __data1_content() {
    return "Компания ВэД работает на рынке оргтехники и средств связи с 1994г. С 1995 года являтся одним из ведущих " +
        "дистрибуторов <a href='https://www.samsung.com'>Samsung Electronics</a> и " +
        "<a href='https://www.panasonic.com'>Panasonic</a>, с 1998 года дистрибутором " +
        "<a href='http://daewoo-electronics.ru'>Daewoo Corporation</a> и ресселером " +
        "<a href='https://www.dlink.com'>D-Link</a>, а также представляет продукцию более 20 фирм-производителей " +
        "таких, как: <a href='https://www.canon.ru/'>Canon</a>, <a href='https://w3.siemens.ru/'>Siemens</a>, " +
        "<a href='https://bitprice.ru/brands/sanyo'>Sanyo</a>, <a href='https://www.brother.ru/'>Brother</a>, " +
        "<a href='https://www.lg.com/global'>LG</a>, <a href='https://www.casio.com/'>Casio</a>, " +
        "<a href='https://epson.com/usa'>Epson</a>, <a href='https://www8.hp.com'>HP</a>, " +
        "<a href='https://www.ricoh.com/'>Ricoh</a>, " +
        "<a href='https://www.konicaminolta.com/selector/index.html'>Minolta</a>, " +
        "<a href='https://www.olivetti.com/'>Olivetti</a>, <a href='http://www.toshiba.com/tai/'>Toshiba</a>, " +
        "<a href='https://global.sharp/'>Sharp</a>, <a href='https://www.verbatim.com/global?gip=1'>Verbatim</a>, " +
        "<a href='http://www.fullmark.com/'>Fullmark</a>, <a href='https://www.intel.com'>Intel</a>, " +
        "<a href='https://www.microsoft.com'>Microsoft</a>, <a href='https://en.wikipedia.org/wiki/3Com'>3COM</a> " +
        "и др. <a href='' id='more_link'>подробнее...</a>";
}

function data1() {
    return "<div style='padding-right: 20px'><br>" + __data1_title() + __data1_content() + "</div><br><br>";
}


function data2() {
    return "<br><table id='data2_table' style='width: 100%; padding-right: 20px'>" +
        "  <tr>" +
        "    <td class='data_block_header'>DCS Compact II</td>" +
        "    <td class='data_block_header'>Samsung iDCS 500</td>" +
        "  </tr>" +
        "  <tr>" +
        "    <td><img src='img/compactii.jpg' alt=''></td>" +
        "    <td><img src='img/idcs500_.gif' alt=''></td>" +
        "  </tr>" +
        "  <tr>" +
        "    <td class='data_block_footer'>большие возможности в компактной упаковке</td>" +
        "    <td class='data_block_footer'>новая, высокотехнологичная модель с широкими возможностями интеграции услуг голоса и данных.</td>" +
        "  </tr>" +
        "</table>";
}


function __news_item(date, text, link, date_color) {
    return "<div>" +
        "<span class='data_block_news_item_date' style='width: min-content; height: min-content; background-color: " +
        date_color + ";'>" + date + "</span> " +
        "<span class='data_block_news_item_text'>" + text +
        "</span> <a class='data_block_news_item_more' href='" + link + "'>подробнее...</a>" +
        "</div>";
}

function __news_list(items, date_color) {
    result = "";
    for (var i = 0; i < items.length; i++) {
        var it = items[i];
        result += __news_item(it[0], it[1], it[2], date_color);
        result += "<br><div class='data_block_news_item_divider'></div>";
    }
    return result;
}

function make_news() {
    return "<div style='background-color: #EEEEEE; margin-right: 20px; padding: 5px'><br>" + __news_list([
        ["19/09/03", "Появились в продаже телефоны Panasonic KX-TMC40RU с автоответчиком и KX-TCD400RU стандарта DECT.",
            ""],
        ["12/09/03", "Новые предложения в разделе \"Распродажа\" отдела телекоммуникаций",
            ""],
        ["03/05/03", "В продаже появились факсы Panasonic серии KX-FT78 белого цвета - KX-FT78RUW",
            ""],
        ["20/01/03", "Завершилась сертификация АТС Samsung серии DCS: iDCS 500, DCS, DCS Compact II, DCS VIP. " +
        "Получен Сертификат Соответствия Минестервства связи.",
            ""]
    ], "#C8C8C8") + "<br></div>";
}

function make_press_release() {
    return "<div style='background-color: #EEEEEE; margin-right: 20px; padding: 5px'><br>" + __news_list([
        ["19/09/03", "Третья конференция региональных партнеров ВэД",
            ""],
        ["12/09/03", "Компания ВэД приглашает посетить выставку \"Связь-Экспокомм\"",
            ""],
        ["03/05/03", "Компания ВэД провела вторую ежегодную конференцию региональных партнеров",
            ""],
        ["20/01/03", "Компания ВэД получила статус поставщика программного обеспечения для решения маркетинговых " +
        "задач компании Frito Lay",
            ""]
    ], "#C19200") + "<br></div>";
}


function make_subscription() {
    return "<div style='background-color: #EEEEEE; padding-left: 20px; margin-top: -10px; margin-right: 20px'><br>" +
        "<p style='color: #396896'>Ваш e-mail:</p>" +
        "<form>" +
        "  <input type='email' style='width: 180px'>" +
        "  <select>" +
        "    <option>подписаться</option>" +
        "    <option>отписаться</option>" +
        "  </select>" +
        "  <button>ok!</button>" +
        "</form>" +
        "<br></div>";
}
