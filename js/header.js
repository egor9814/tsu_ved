
function __enter(btn_name) {
    btn = document.getElementById(btn_name);
    btn.style.opacity = "0.7";
    btn.style.filter = 'alpha(opacity=70)';
}
function __leave(btn_name) {
    btn = document.getElementById(btn_name);
    btn.style.opacity = "1.0";
    btn.style.filter = 'alpha(opacity=100)';
}
function __click(msg) {
    alert(msg);
}


function home_enter() {
    __enter("home_btn");
}
function home_leave() {
    __leave("home_btn");
}
function home_click() {
    __click("Home button pressed!");
}

function search_enter() {
    __enter("search_btn");
}
function search_leave() {
    __leave("search_btn");
}
function search_click() {
    __click("Search button pressed!");
}

function letter_enter() {
    __enter("letter_btn");
}
function letter_leave() {
    __leave("letter_btn");
}
function letter_click() {
    __click("Letter button pressed!");
}
